<h3 style="text-align: center;text-shadow: 2px 2px #ddd;">Please login</h3>

<form method="post">
	<div style="margin: auto; text-align: left; display: inline-block">
		<div style="width: 80px; display:inline-block;color: #444">Email</div>
		<input type="email" name="email" style="padding: 5px" >
	</div>
	
	<br>

	<div style="margin: auto; margin-top:20px; text-align: left; display: inline-block">
		<div style="width: 80px; display:inline-block;color: #444">Password</div>
		<input type="password" name="password" style="padding: 5px" required>
	</div>
	
	<br><br>
	
	<input type="submit" style="width: 150px; padding: 5px 20px" value="Go" required>
	
</form>

<br><br>

<a href="?registrationForm=true" style="text-decoration:none;color: #444">New User</a>

<br><br>

<a href="?forgotPasswordForm=true" style="text-decoration:none;color: #444">Forgot password</a>