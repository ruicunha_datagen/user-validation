<h3 style="text-align: center;text-shadow: 2px 2px #ddd;">Reset your password</h3>
<form method="post">

	<input type="hidden" name="validationKey" value="<?php echo $this->validationKey ?>">
	<input type="hidden" name="validationToken" value="<?php echo $this->validationToken ?>">

	<div style="margin: auto;  text-align: left; display: inline-block">
		<div style="width: 150px; display:inline-block;color: #444">Email</div>
		<input type="email" name="emailResetPassword" style="padding: 5px" required>
	</div>
	
	<br>

	<div style="margin: auto; margin-top:20px; text-align: left; display: inline-block">
		<div style="width: 150px; display:inline-block;color: #444">Password</div>
		<input type="password" name="password" style="padding: 5px" required>
	</div>
	
	<br>

	<div style="margin: auto; margin-top:20px; text-align: left; display: inline-block">
		<div style="width: 150px; display:inline-block;color: #444">Confirm password</div>
		<input type="password" name="passwordConfirmation" style="padding: 5px" required>
	</div>
	
	<br><br>
	
	<input type="submit" style="width: 150px; padding: 5px 20px" value="Go" required>
	
</form>

<br><br>

<a href="/" style="text-decoration:none;color: #444">Login</a>
