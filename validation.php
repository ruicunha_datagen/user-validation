<?php


class UserValidation
{
	public $pathToValidationForm;
	public $pathToForgotPasswordForm;
	public $pathToRegistrationForm;
	public $pathToPasswordResetForm;
	
	public $databaseConnectionRO;
	public $databaseConnectionRW;
	
	public $email;
	public $password;
	public $passwordConfirmation;
	
	public $validationKey;
	public $validationToken;
	

	/*
	Run this method before using the class.*/
	public function installClass(){
		$this->createUserDatabase();
	}
	
	public function getValidationForm(){
		ob_start();
		include $this->pathToValidationForm;
		$form = ob_get_contents();
		ob_clean();
		return $form;
	}
	public function getForgotPasswordForm(){
		ob_start();
		include $this->pathToForgotPasswordForm;
		$form = ob_get_contents();
		ob_clean();
		return $form;
	}
	public function getRegistrationForm(){
		ob_start();
		include $this->pathToRegistrationForm;
		$form = ob_get_contents();
		ob_clean();
		return $form;
	}
	public function getResetPasswordForm(){
		$user=$this->getUserByEmail( $this->email );
		if( !is_array($user) ){
			return array(
				"status"=>false,
				"message"=>'User not found.'
			);
		}
		$this->validationKey=rand(9999,999999);
		$this->validationToken=$this->getUrlToken( $this->validationKey, $user['passwordHash'] );
		
		ob_start();
		include $this->pathToPasswordResetForm;
		$form = ob_get_contents();
		ob_clean();
		return $form;
	}
	
	public function validateForgotPasswordData(){
		
		if( empty( $this->password) || $this->password!==$this->passwordConfirmation){
			return array(
				"status"=>false,
				"message"=>'Invalid password.'
			);
		}
		
		$user=$this->getUserByEmail( $this->email );
		if( !is_array($user) ){
			return array(
				"status"=>false,
				"message"=>'User not found.'
			);
		}
		$token=$this->getUrlToken( $this->validationKey, $user['passwordHash'] );
		if( $token===$this->validationToken ){
			$result=$this->changePassword( $this->email, $this->password );
			if ( $result['status']===true ){
				die("<center>Password changed with success.</center>");
			}else{
				return array(
					"status"=>false,
					"message"=>'There was an unknown error.'
				);
			}
			
		}else{
			return array(
				"status"=>false,
				"message"=>'That data has no power here.'
			);
		}
		
	}
	
	public function activateNewAccount(){
		
		$user=$this->getUserByEmail( $this->email );
		$token = $this->getUrlToken($this->validationKey, $user['passwordHash']);
		if( $token===$this->validationToken ){
			$result=$this->updateActivationDate( $this->email );
			if( $result['status']===true){
				return array(
					"status"=>true,
					"message"=>'This account has been validated.'
				);
			}else{
				return array(
					"status"=>true,
					"message"=>'An unknown error has occured.'
				);
			}
		}else{
			return array(
				"status"=>false,
				"message"=>'That link has no power here.'
			);
		}
	}
	
	public function createNewUser(){
		
		$hash= password_hash( $this->password, PASSWORD_DEFAULT);
		
		try{		
			$queryString="
				INSERT INTO users
				(email, passwordHash)
				values
				(:_email, :_hash)
				";
			$query = $this->databaseConnectionRW -> prepare($queryString);
			$query->bindParam(':_email',	$this->email , PDO::PARAM_STR);
			$query->bindParam(':_hash',	$hash , PDO::PARAM_STR);
			$query -> execute();
			if( $query->rowCount()===1 ){
				$key=rand(9999,999999);
				$token=$this->getUrlToken($key, $hash);
				return array(
					"status"=>true,
					"message"=>'User was created.',
					"new_user_id"=>$this->databaseConnectionRW->lastInsertId(),
					"validation"=>array('key'=>$key , 'token'=>$token)
				);
			}else{
				throw new PDOException("There was an undetermined error.");
			}
		}catch(PDOException $e){
			return array(
				"status"=>false,
				"message"=>$e->getMessage()
			);
		}
	}
	
	public function validateLoginData(){
		
		try{		

			$user=$this->getUserByEmail( $this->email );
			
			if( is_array($user) && password_verify($this->password, $user['passwordHash']) ){
				
				$cancelation=strtotime($user['dateCancelation']);
				$now=time();

				// if its not validated yet
				if( empty($user['dateValidation']) or $user['dateValidation']==='0000-00-00 00:00:00'  ){
					
					$key=rand(9999,999999);
					$token=$this->getUrlToken($key, $user['passwordHash']);
					return array(
						"status"=>false,
						"message"=>"This account is waiting email validation.",
						"validation"=>array('key'=>$key , 'token'=>$token)
					);
				}
				elseif( $user['dateCancelation']!=='0000-00-00 00:00:00' && strlen($user['dateCancelation'])>0 && $cancelation<$now ){
					return array(
						"status"=>false,
						"message"=>"This account has been canceled",
						"cancelationDate"=>$user['dateCancelation']
					);
				}
				else{
					$loginCount=$user['numberOfLogins'] + 1;
					$this->updateUserRecordWithLoginInfo($loginCount);
					return array(
						"status"=>true,
						"message"=>'User is valid',
						"userData"=>$user
					);
				}
			}else{
				return array(
					"status"=>false,
					"message"=>'Wrong credentials.'
					);
			}
		}catch(PDOException $e){
			return array(
				"status"=>false,
				"message"=>$e->getMessage()
			);
		}
		
	}
	
	public function createUserDatabase(){
		try{		
			$queryString="
				CREATE TABLE IF NOT EXISTS `users` (
					`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
					`email` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
					`passwordHash` VARCHAR(9999) NOT NULL COLLATE 'utf8mb4_general_ci',
					`passwordLastChanged` DATETIME NOT NULL,
					`numberOfLogins` INT(10) UNSIGNED NOT NULL,
					`dateCreation` DATETIME NOT NULL DEFAULT current_timestamp(),
					`dateValidation` DATETIME NULL DEFAULT NULL,
					`dateLastLogin` DATETIME NULL DEFAULT NULL,
					`dateLastUpdate` DATETIME NULL DEFAULT NULL ON UPDATE current_timestamp(),
					`dateCancelation` DATETIME NULL DEFAULT NULL,
					PRIMARY KEY (`id`) USING BTREE,
					UNIQUE INDEX `email` (`email`) USING BTREE
				)
				COLLATE='utf8mb4_general_ci'
				ENGINE=InnoDB
				AUTO_INCREMENT=2;
				";
			$this->databaseConnectionRW -> exec($queryString);
			
			return array(
				"status"=>true,
				"message"=>'database was successfully created'
			);
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}
	
	private function getUrlToken($key, $passwordHash){
		$hash = crypt( $key . $passwordHash, "Always_use_salt.");
		$token=substr($hash, 0,100) ;
		$token=preg_replace('/[^a-zA-Z0-9]/', '', $token);
		return $token;
	}
	
	private function updateUserRecordWithLoginInfo($loginCount){
		try{		
			$queryString="
				UPDATE users
				SET	numberOfLogins= :_logincount,
					dateLastLogin= :_now
				WHERE email= :_email				
				";
			$now= date('Y-m-d H:i:s');
			$query = $this->databaseConnectionRW -> prepare($queryString);
			$query->bindParam(':_email',	$this->email , PDO::PARAM_STR);
			$query->bindParam(':_now', $now , PDO::PARAM_STR);
			$query->bindParam(':_logincount',	$loginCount , PDO::PARAM_INT);
			$query -> execute();
			return array(
				"status"=>true,
				"message"=>'User information updated with new login',
				"loginCount"=>$loginCount
			);
		}catch(PDOException $e){
			return array(
				"status"=>false,
				"message"=>$e->getMessage()
			);
		}
	}
	
	private function getUserByEmail( $email ){
		try{
			$queryString="
				SELECT *
				FROM users
				WHERE email= :_email				
				";
			$query = $this->databaseConnectionRO -> prepare($queryString);
			$query->bindParam(':_email', $email , PDO::PARAM_STR);
			$query -> execute();
			return $query->fetch(PDO::FETCH_ASSOC);
		}catch( PDOException $e ){
			return array(
				"status"=>false,
				"message"=>$e->getMessage()
			);
		}
	}

	private function updateActivationDate( $email ){
		try{		
			$queryString="
				UPDATE users
				SET	dateValidation= :_dateValidation
				WHERE email= :_email
				AND ( dateValidation IS NULL OR dateValidation='0000-00-00 00:00:00' )
				";
			$now= date('Y-m-d H:i:s');
			$query = $this->databaseConnectionRW -> prepare($queryString);
			$query->bindParam(':_email', $email , PDO::PARAM_STR);
			$query->bindParam(':_dateValidation', $now , PDO::PARAM_STR);
			$query -> execute();
			return array(
				"status"=>true,
				"message"=>'User information updated with validation date'
			);
		}catch(PDOException $e){
			return array(
				"status"=>false,
				"message"=>$e->getMessage()
			);
		}
	}

	public function changePassword( $email, $password ){
		$hash= password_hash( $password, PASSWORD_DEFAULT);		try{		
		$queryString="
				UPDATE users
				SET	passwordHash= :_hash
				WHERE email= :_email				
				";
			$query = $this->databaseConnectionRW -> prepare($queryString);
			$query->bindParam(':_email',	$this->email , PDO::PARAM_STR);
			$query->bindParam(':_hash', $hash , PDO::PARAM_STR);
			$query -> execute();
			return array(
				"status"=>true,
				"message"=>'User password updated.'
			);
		}catch(PDOException $e){
			return array(
				"status"=>false,
				"message"=>$e->getMessage()
			);
		}
	}
	
}




?>
