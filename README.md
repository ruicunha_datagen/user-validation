###### Author: Rui Cunha
###### Email: r.cunha@datagen.eu

### About this project
This class aims to provide a complete set of resources to handle:
- Registration and validation of new users
- Login of existing users
- Reseting forgotten passwords
It features:
- A method to create the required table structure
- Passworh hashing
- Token secured validation
- Methods to include the bundled forms in your HTML code

###### How to set it up
Include the file validation.php, if you are not using composer. If you are using composer, just type:
```shell
composer require hotworks/user-validation
```
2. Copy the four forms to a destination of your choice, you will be providing their path later.
3. Create the user table on your database. You have a method for this in the class:
```php
$validation=new UserValidation();
$validation->databaseConnectionRW = $myPdoConnectionObject ;
$validation->createUserDatabase();
```

###### How to display the forms
The class has a property for the path of each one of the forms:
```php
public string $pathToValidationForm;
public string $pathToForgotPasswordForm;
public string $pathToRegistrationForm;
public string $pathToPasswordResetForm;
```
It also has a method that returns the HTML markup of each form:
```php
public function getValidationForm(){...};
public function getForgotPasswordForm(){...};
public function getRegistrationForm(){...};
public function getResetPasswordForm(){...};
```
To access the html, provide the path and call the method:
```php
$validation=new UserValidation();
$validation->pathToValidationForm = "/path/to/my/validation/form";
$form=$validation-> getValidationForm();
echo $form;
```

###### How to handle the posted data
When the forms are submited, data is posted to the server.
### Creating a new user
```php
$validation=new UserValidation();
$validation->email = $_POST['email'];
$validation->password = $_POST['password'];
$validation->passwordConfirmation = $_POST['passwordConfirmation'];
$result = $validation->createNewUser();
/*
If the user is created, $result will contain a key and a token. You need to build a URL with them and email it to your user, so that he can validate his new account.
*/
```
### Triggering the new account validation
```php
$validation=new UserValidation();
$validation->email = $_GET['email'];
$validation->validationKey = $_GET['key'];
$validation->validationToken = $_GET['token'];
$result=$validation->activateNewAccount();
```
### Loging an existing user
```php
$validation=new UserValidation();
$validation->email = $_POST['email'];
$validation->password = $_POST['password'];
$result = $validation->validateLoginData();
```
### Accessing the password reset request form
This one has a detail: you need to provide the email via the 'email' property. The key and token that will be generated within the form will depend on this.
```php
$validation=new UserValidation();
$validation->email = $_GET['emailForgotPassword'];
$validation->pathToPasswordResetForm = "/path/to/my/password/reset/form";
form = $validation->getResetPasswordForm();
echo $form;
/*
This form will contain hidden key and token that will be posted as well.
*/
```
### Handling the password reset request form data
```php
$validation=new UserValidation();
$validation->email = $_POST['emailResetPassword'];
$validation->password = $_POST['password'];
$validation->passwordConfirmation = $_POST['passwordConfirmation'];
$validation->validationKey = $_POST['validationKey'];
$validation->validationToken = $_POST['validationToken'];
$result=$validation->validateForgotPasswordData();
/*
If everything is ok , the password will be updated
*/
```