<?php

include "validation.php";

$validation= new UserValidation();


$validation->databaseConnectionRW = new PDO(
		"mysql:	host=localhost;
				dbname=test;
				charset=utf8",
		"root" ,
		'' ,
		array(	PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
				PDO::ATTR_EMULATE_PREPARES => false)
	 
);
$validation->databaseConnectionRO = $validation->databaseConnectionRW;

$validation->createUserDatabase();


$message="";
$form="";

// handles form submissions
if( !empty($_POST) ){
	// if it is a password reset
	if(	!empty($_POST['emailResetPassword']) &&
		!empty($_POST['password']) && 
		!empty($_POST['passwordConfirmation']) &&
		!empty($_POST['validationKey']) &&
		!empty($_POST['validationToken'])
		){
		$validation->email = $_POST['emailResetPassword'];
		$validation->password = $_POST['password'];
		$validation->passwordConfirmation = $_POST['passwordConfirmation'];
		$validation->validationKey = $_POST['validationKey'];
		$validation->validationToken = $_POST['validationToken'];
		$result=$validation->validateForgotPasswordData();
		if ($result['status']===true){
			$message="Password was changed.";
		}else{
			$message="There was an unknown error.";
		}
	}
	// if it is data from the registration form
	elseif(	!empty($_POST['email']) && 
			!empty($_POST['password']) && 
			!empty($_POST['passwordConfirmation'])){
		
		if( $_POST['password']!==$_POST['passwordConfirmation']){
			$message="Password and confirmation need to be same";
		}
		$validation->email = $_POST['email'];
		$validation->password = $_POST['password'];
		$validation->passwordConfirmation = $_POST['passwordConfirmation'];
		$result = $validation->createNewUser();
		if ($result['status']===true){
			/*
			We need to provide a link to the user, so that he can validate his account. This link should be emailed to the user, to confirm ownership of the email. Since this is just an example implementation, we will provide the link on the screen. */
			$link=	"?email=" . $validation->email .
					"&key=".$result['validation']['key'].
					"&token=".$result['validation']['token'];
			$message="New user created. Click <b><a href='$link'>here</a></b> to validate your new account";
		}else{
			$message="There was an error creating this user. Please try a password reset.";
		}
	
	}
	// if it is data from the login form
	elseif(	!empty($_POST['email']) &&
			!empty($_POST['password']) ){
	
		$validation->email = $_POST['email'];
		$validation->password = $_POST['password'];
		$result = $validation->validateLoginData();
		if ( $result['status']===true ){
			die("<center>Validation correct</center>");
		}else{
			$message=$result['message'];
			// if the account has not been validated yet
			if(!empty($result['validation'])){
				$link=	"?email=" . $validation->email .
						"&key=".$result['validation']['key'].
						"&token=".$result['validation']['token'];
				$message.="Click <b><a href='$link'>here</a></b> to validate.";
			}
		}
	}

}


// selects the form that will be displayed
if (!empty( $_GET['forgotPasswordForm'] )){
	$validation->pathToForgotPasswordForm= __DIR__ . DIRECTORY_SEPARATOR . "forgotPasswordForm.php";
	$form=$validation->getForgotPasswordForm();
}
elseif( !empty( $_GET['registrationForm']) ){
	$validation->pathToRegistrationForm= __DIR__ . DIRECTORY_SEPARATOR . "registrationForm.php";
	$form=$validation->getRegistrationForm();
}
elseif( !empty( $_GET['email']) && !empty( $_GET['key']) && !empty( $_GET['token']) ){
	$validation->email = $_GET['email'];
	$validation->validationKey = $_GET['key'];
	$validation->validationToken = $_GET['token'];
	$result=$validation->activateNewAccount();
	$message=$result['message'];
}
elseif( !empty( $_GET['resetPasswordForm']) && !empty($_POST['emailForgotPassword']) ){
	$validation->email = $_POST['emailForgotPassword'];
	/* normally, you would email a link to your user, so that he can access the reset page from his inbox. That page will include hidden key and token, automatically generated. You just need to provide the email of the user who is requiring the reset.
	Since this is just an example, we will move straight to that page. */
	$validation->pathToPasswordResetForm= __DIR__ . DIRECTORY_SEPARATOR . "resetPasswordForm.php";
	$form = $validation->getResetPasswordForm();
	if( isset($form['status']) && $form['status']===false){
		$message="User not found.";
		$validation->pathToForgotPasswordForm= __DIR__ . DIRECTORY_SEPARATOR . "forgotPasswordForm.php";
		$form=$validation->getForgotPasswordForm();
	}
}
else{
	$validation->pathToValidationForm= __DIR__ . DIRECTORY_SEPARATOR . "validationForm.php";
	$form=$validation->getValidationForm();
}



?>
<!DOCTYPE html><html>
<head>
  <meta charset="UTF-8">
  <meta name="description" content="Free Web tutorials">
  <meta name="keywords" content="HTML, CSS, JavaScript">
  <meta name="author" content="John Doe">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<div style="font-weight: bold; position: absolute; width: 100%;color: red; text-align:center;top: 0">
	<?php echo $message ?>
</div>
<div style="text-align:center; font-family: arial; font-weight: bold;font-size: 16px; max-width: 400px;margin: auto; margin-top:10vh;">
	<?php echo $form ?>

</div>
</body>
</html>